Trường Đại học Khoa học tự nhiên<br>
Khoa Công nghệ thông tin<br>
Bộ môn Hệ điều hành
# HƯỚNG DẪN SỬ DỤNG
## Phần 1
Sau khi tải mã nguồn về và giải nén, sau đó dùng cd để di chuyển đến thư mục này.<br>
Kiểm tra xem trong thư mục đã đủ file chưa bằng lệnh:<br>
    `$ ls`

Nếu kết quả gồm thư mục module và file test.c thì thực hiện lệnh dưới để khỏi nhập sudo cho các câu lệnh tiếp theo:<br>
    `$ sudo -s`

Vào thư mục Module bằng:<br>
    `# cd Modules/`

Tạo kernel module bằng:<br>
    `# make`

Thêm module vào trong kernel bằng:<br>
    `# insmod modules.ko`

Kiểm tra module hoạt động chưa: <br>
    `# cat /dev/myrandom`

Nếu kết quả trả về 1 số tự nhiên nhỏ hơn 10 thì module đang hoạt động. Tiếp theo ta thay đổi thuộc tính của file myrandom để user space có thể đọc và viết: <br>
    `# chmod 666 /dev/myrandom`

Tiếp theo ta tắt sudo -s bằng:<br>
    `# exit`

Biên dịch file test.c để tạo ra file a.out:<br>
    `$ gcc test.c`

Chạy file a.out:<br>
    `$ ./a.out`


## Phần 2