#include <stdio.h>
#include <linux/kernel.h>
#include <sys/syscall.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>

int main()
{
    int fd = syscall(2, "test_file.txt", O_RDONLY);
    if (fd == -1)
    {
        // print which type of error have in a code
        printf("Error\n");
    }
    else
    {
        printf("fd = %d \n", fd);
        char *buf = "Test Done !";
        syscall(1, fd, buf, strlen(buf));
    }
    return 0;
}
