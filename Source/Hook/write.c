#include <asm/unistd.h>
#include <asm/cacheflush.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/syscalls.h>
#include <asm/pgtable_types.h>
#include <linux/highmem.h>
#include <linux/fs.h>
#include <linux/sched.h>
#include <linux/moduleparam.h>
#include <linux/unistd.h>
#include <asm/cacheflush.h>
MODULE_LICENSE("GPL");
MODULE_AUTHOR("75-82");

void **system_call_table_addr;
/*my custom syscall that takes process name*/
asmlinkage int (*custom_syscall)(int fd, const void *buf, size_t count);
/*hook*/
// Hook function, replace the system call WRITE
// Take the same parameter as the system call WRITE
ssize_t hook_function(int fd, const void *buf, size_t count)
{
 // Print calling process name
 printk(KERN_INFO "Calling process:%s\n",current->comm);
 char pathname[255];
 fd_to_pathname(fd,pathname);
 printk(KERN_INFO "Written file: %s\n",pathname);
 int written_bytes = custom_syscall(fd, buf, count);
 printk(KERN_INFO "Number of written bytes:%d\n",written_bytes);
 return written_bytes;
}

/*Make page writeable*/
int make_rw(unsigned long address)
{
    unsigned int level;
    pte_t *pte = lookup_address(address, &level);
    if (pte->pte & ~_PAGE_RW)
    {
        pte->pte |= _PAGE_RW;
    }
    return 0;
}
/* Make the page write protected */
int make_ro(unsigned long address)
{
    unsigned int level;
    pte_t *pte = lookup_address(address, &level);
    pte->pte = pte->pte & ~_PAGE_RW;
    return 0;
}
static int __init entry_point(void)
{ //system call table address
    system_call_table_addr = (void *)0xffffffff9c6001a0;
    // Assign custom_syscall to system call WRITE
    custom_syscall = system_call_table_addr[__NR_write];
    //Disable page protection
    make_rw((unsigned long)system_call_table_addr);
    // Replace system call WRITE by our Hook function
    system_call_table_addr[__NR_write] = hook_function;
    return 0;
}
static void __exit exit_point(void)
{
    // Restore system call WRITE
    system_call_table_addr[__NR_write] = custom_syscall;
    //Renable page protection
    make_ro((unsigned long)system_call_table_addr);
}
module_init(entry_point);
module_exit(exit_point);

