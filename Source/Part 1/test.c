#include <unistd.h> //open, close | always first, defines compliance
#include <fcntl.h>  //O_RDONLY
#include <stdio.h>
#include <stdlib.h> //printf
#include <string.h>

int main()
{
    char buff;
    int choose;
    do
    {
        printf("Enter your choose:\n0 . Exit!\n1 . Read file\n");
        scanf("%d", &choose);
        if (choose)
        {
            int to_read = 1; //byte want to read

            FILE *fileread;

            fileread = fopen("/dev/myrandom", "r");

            if (!fileread)
                printf("Error !");

            int retal = fread(&buff, sizeof(char), to_read, fileread);
            fclose(fileread);
            printf("Read %d byte form device. Random number is [%c]\n", to_read, buff);
        }
        else
        {
            printf("Bye Bye !");
            exit(0);
        }
    } while (choose != 0);
    return 0;
}
