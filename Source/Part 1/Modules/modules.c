#include <linux/module.h>
#include <linux/version.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/kdev_t.h>
#include <linux/fs.h>
#include <linux/device.h>
#include <linux/cdev.h>
#include <linux/uaccess.h>
#include <linux/random.h>



static dev_t first; // Global variable for the first device number
static struct cdev c_dev; // Global variable for the character device structure
static struct class *cl;


unsigned char get_random_byte(int max)
{
    unsigned char c;
    get_random_bytes(&c,sizeof(c));
    return c%max;
}



static int my_open(struct inode *i, struct file *f)
{
    printk(KERN_INFO "Driver: open()\n");
    return 0;
}

static int my_close(struct inode *i, struct file *f)
{
    printk(KERN_INFO "Driver: close()\n");
    return 0;
}

static ssize_t my_write(struct file *f, const char __user *buf,size_t len, loff_t *off)
{
    // Khong lam gi het vi de khong yeu cau ghi file tu user space
}

static ssize_t my_read(struct file *f, char __user *buf, size_t len, loff_t *off)
{

    printk(KERN_INFO "Driver: read()\n");

    if(*off>0)
    {
        printk("Stop read! Closing file...!\n");
        return 0;
    }
    
    char c;
  

    c=get_random_byte(10)+'0';


    int retval=copy_to_user(buf,&c,1);

    if(retval == 0)
    {
         printk("copy_to_user returned -> Successfully");
    }

    *off+=1;

    return 1;
}


static struct file_operations pugs_fops =
{
    .owner = THIS_MODULE,
    .open = my_open,
    .release = my_close,
    .read = my_read,
    .write = my_write
};


static int __init ofcd_init(void) /* Constructor */
{
    printk(KERN_INFO "hihi: ofcd registered");
    if (alloc_chrdev_region(&first, 0, 1, "Shweta") < 0)
    {
        return -1;
    }
    if ((cl = class_create(THIS_MODULE, "chardrv")) == NULL)
    {
        unregister_chrdev_region(first, 1);
        return -1;
    }
    if (device_create(cl, NULL, first, NULL, "myrandom") == NULL)
    {
        class_destroy(cl);
        unregister_chrdev_region(first, 1);
        return -1;
    }
    cdev_init(&c_dev, &pugs_fops);
    if (cdev_add(&c_dev, first, 1) == -1)
    {
        device_destroy(cl, first);
        class_destroy(cl);
        unregister_chrdev_region(first, 1);
        return -1;
    }


    return 0;
}
static void __exit ofcd_exit(void) /* Destructor */
{
    cdev_del(&c_dev);
    device_destroy(cl, first);
    class_destroy(cl);
    unregister_chrdev_region(first, 1);
    printk(KERN_INFO "hihi: ofcd unregistered");
}


module_init(ofcd_init);
module_exit(ofcd_exit);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Cong Anh - Van Anh");
MODULE_DESCRIPTION("Our First Character Driver");
